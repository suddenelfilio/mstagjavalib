package net.suddenelfilio.ws.tag;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public final class URITag extends Tag
{

	public URITag()
	{
		TagStatus = Status.Active;
		Types = ImageTypes.Jpeg;
	}

	public String	MedFiUrl;
	public String	CurrentTitle;

	public URL ToURL(URLType type) throws Exception
	{

		if (MSTagService.SVCAPITOKEN == null || MSTagService.SVCAPITOKEN.equals(""))
			throw new Exception("Did you forget to set the MSTagService.SVCAPITOKEN with your token?");
		Map<String, String> mp = new HashMap<String, String>();
		mp.put("at", MSTagService.SVCAPITOKEN);
		switch (type)
		{
			case CREATE :

				if (IsNullOrEmpty(CategoryName))
					throw new Exception("The CategoryName cannot be null or empty when creating a Tag.");
				else
					mp.put("cn", CategoryName);

				if (IsNullOrEmpty(InteractionNote))
					mp.put("in", "");
				else
					mp.put("in", InteractionNote);

				if (IsNullOrEmpty(MedFiUrl))
					throw new Exception("The MedFiUrl cannot be null or empty when creating a Tag.");
				else
					mp.put("mfu", MedFiUrl);

				if (TagStatus == null)
					mp.put("ts", "");
				else
					mp.put("ts", TagStatus.toString());

				if (IsNullOrEmpty(Title))
					throw new Exception("The Title cannot be null or empty when creating a Tag.");
				else
					mp.put("t", Title);

				if (IsNullOrEmpty(UTCEndDate))
					mp.put("ued", "");
				else
					mp.put("ued", UTCEndDate);

				if (IsNullOrEmpty(UTCStartDate))
					mp.put("usd", "");
				else
					mp.put("usd", UTCStartDate);

				return new URL(GenerateURL(mp, "CreateURITag"));

			case EDIT :

				if (IsNullOrEmpty(CurrentTitle))
					throw new Exception("The CurrentTile cannot be null or empty when editing a Tag.");
				else
					mp.put("ct", CurrentTitle);

				if (IsNullOrEmpty(CategoryName))
					mp.put("cn", "");
				else
					mp.put("cn", CategoryName);

				if (IsNullOrEmpty(InteractionNote))
					mp.put("in", "");
				else
					mp.put("in", InteractionNote);

				if (IsNullOrEmpty(MedFiUrl))
					throw new Exception("The MedFiUrl cannot be null or empty when editing a Tag.");
				else
					mp.put("mfu", MedFiUrl);

				if (TagStatus == null)
					mp.put("ts", "");
				else
					mp.put("ts", TagStatus.toString());

				if (IsNullOrEmpty(Title))
					mp.put("t", "");
				else
					mp.put("t", Title);

				if (IsNullOrEmpty(UTCEndDate))
					mp.put("ued", "");
				else
					mp.put("ued", UTCEndDate);

				if (IsNullOrEmpty(UTCStartDate))
					mp.put("usd", "");
				else
					mp.put("usd", UTCStartDate);

				return new URL(GenerateURL(mp, "EditURITag"));
			case ACTIVE :
				return Activate();
			case PAUSE :
				return Pause();

		}
		return null;
	}
}
