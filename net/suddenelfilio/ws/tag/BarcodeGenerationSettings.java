package net.suddenelfilio.ws.tag;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class BarcodeGenerationSettings extends TagItemAPIBase
{

	public BarcodeGenerationSettings()
	{
		ImageType = ImageTypes.Jpeg;
		Size = 0.25;
		Decoration = TagDecorations.HCCBRP_DECORATION_DOWNLOAD;
		IsBlackAndWhite = false;
	}

	public String			CategoryName;
	public String			TagTitle;
	public ImageTypes		ImageType;
	public Double			Size;
	public TagDecorations	Decoration;
	public Boolean			IsBlackAndWhite;

	@Override
	public URL ToURL(URLType type) throws MalformedURLException, Exception
	{
		if (type == URLType.GENERATE)
		{
			if (MSTagService.SVCAPITOKEN == null
					|| MSTagService.SVCAPITOKEN.equals(""))
				throw new Exception("Did you forget to set the MSTagService.SVCAPITOKEN with your token?");

			Map<String, String> mp = new HashMap<String, String>();
			mp.put("at", MSTagService.SVCAPITOKEN);

			if (IsNullOrEmpty(CategoryName))
				throw new Exception("CategoryName cannot be null or empty when generating a barcode.");
			else
				mp.put("cn", CategoryName);

			if (IsNullOrEmpty(TagTitle))
				throw new Exception("TagTitle cannot be null or empty when generating a barcode.");
			else
				mp.put("tn", TagTitle);

			mp.put("it", ImageType.toString().toLowerCase());
			mp.put("s", Size.toString());
			mp.put("dt", Decoration.toString().toUpperCase());
			mp.put("bw", IsBlackAndWhite.toString());

			return new URL(GenerateURL(mp, "GenerateBarcode"));
		}

		return null;
	}

}
