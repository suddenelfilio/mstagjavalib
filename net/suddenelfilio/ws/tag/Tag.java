package net.suddenelfilio.ws.tag;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public abstract class Tag extends TagItemAPIBase
{

	public String		InteractionNote;
	public Status		TagStatus;
	public String		Title;
	public ImageTypes	Types;
	public String		UTCEndDate;
	public String		UTCStartDate;
	public String		CategoryName;

	@Override
	public abstract URL ToURL(URLType type) throws MalformedURLException,
			Exception;

	protected final URL Activate() throws Exception
	{
		if (MSTagService.SVCAPITOKEN == null || MSTagService.SVCAPITOKEN.equals(""))
			throw new Exception("Did you forget to set the MSTagService.SVCAPITOKEN with your token?");
		Map<String, String> mp = new HashMap<String, String>();
		mp.put("at", MSTagService.SVCAPITOKEN);

		if (IsNullOrEmpty(Title))
			throw new Exception("The Title cannot be null or empty when activating a Tag.");
		else
			mp.put("tt", Title);

		if (IsNullOrEmpty(CategoryName))
			throw new Exception("The CategoryName cannot be null or empty when activating a Tag.");
		else
			mp.put("cn", CategoryName);

		return new URL(GenerateURL(mp, "ActivateTag"));
	}

	protected final URL Pause() throws Exception
	{
		if (MSTagService.SVCAPITOKEN == null || MSTagService.SVCAPITOKEN.equals(""))
			throw new Exception("Did you forget to set the MSTagService.SVCAPITOKEN with your token?");
		Map<String, String> mp = new HashMap<String, String>();
		mp.put("at", MSTagService.SVCAPITOKEN);

		if (IsNullOrEmpty(Title))
			throw new Exception("The Title cannot be null or empty when pausing a Tag.");
		else
			mp.put("tt", Title);

		if (IsNullOrEmpty(CategoryName))
			throw new Exception("The CategoryName cannot be null or empty when pausing a Tag.");
		else
			mp.put("cn", CategoryName);

		return new URL(GenerateURL(mp, "PauseTag"));
	}

}
