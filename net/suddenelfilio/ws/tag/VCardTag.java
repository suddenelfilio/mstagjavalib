package net.suddenelfilio.ws.tag;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public final class VCardTag extends Tag
{

	public VCardTag()
	{
		TagStatus = Status.Active;
		Types = ImageTypes.Jpeg;
	}

	public String	City;
	public String	Company;
	public String	Country;
	public String	Email;
	public String 	Fax;
	public String	Firstname;
	public String	Lastname;
	public String	MobilePhone;
	public String	State;
	public String	Street;
	public String	Webpage;
	public String	Workphone;
	public String	Zip;
	public String	Password;
	public String	CurrentTitle;

	public URL ToURL(URLType type) throws Exception
	{

		if (MSTagService.SVCAPITOKEN == null || MSTagService.SVCAPITOKEN.equals(""))
			throw new Exception("Did you forget to set the MSTagService.SVCAPITOKEN with your token?");
		Map<String, String> mp = new HashMap<String, String>();
		mp.put("at", MSTagService.SVCAPITOKEN);
		switch (type)
		{
			case CREATE :

				if (IsNullOrEmpty(CategoryName))
					throw new Exception("The CategoryName cannot be null or empty when creating a Tag.");
				else
					mp.put("cn", CategoryName);

				if (IsNullOrEmpty(InteractionNote))
					mp.put("in", "");
				else
					mp.put("in", InteractionNote);

				if (IsNullOrEmpty(City))
					mp.put("c", "");
				else
					mp.put("c", City);

				if (IsNullOrEmpty(Company))
					mp.put("co", "");
				else
					mp.put("co", Company);
				
				if (IsNullOrEmpty(Country))
					mp.put("cou", "");
				else
					mp.put("cou", Country);
				
				if (IsNullOrEmpty(Email))
					mp.put("e", "");
				else
					mp.put("e", Email);
				
				if (IsNullOrEmpty(Fax))
					mp.put("f", "");
				else
					mp.put("f", Fax);
				
				if (IsNullOrEmpty(Firstname))
					mp.put("fn", "");
				else
					mp.put("fn", Firstname);
				
				if (IsNullOrEmpty(Lastname))
					mp.put("ln", "");
				else
					mp.put("ln", Lastname);
				
				if (IsNullOrEmpty(MobilePhone))
					mp.put("mp", "");
				else
					mp.put("mp", MobilePhone);
				
				if (IsNullOrEmpty(State))
					mp.put("st", "");
				else
					mp.put("st", State);
				
				if (IsNullOrEmpty(Webpage))
					mp.put("w", "");
				else
					mp.put("w", Webpage);
				
				if (IsNullOrEmpty(Workphone))
					mp.put("wp", "");
				else
					mp.put("wp", Workphone);
				
				if (IsNullOrEmpty(Zip))
					mp.put("z", "");
				else
					mp.put("z", Zip);
				
				if (IsNullOrEmpty(Street))
					mp.put("str", "");
				else
					mp.put("str", Street);
				
				if (IsNullOrEmpty(Password))
					mp.put("p", "");
				else
					mp.put("p", Password);

				if (TagStatus == null)
					mp.put("ts", "");
				else
					mp.put("ts", TagStatus.toString());

				if (IsNullOrEmpty(Title))
					throw new Exception("The Title cannot be null or empty when creating a Tag.");
				else
					mp.put("t", Title);

				if (IsNullOrEmpty(UTCEndDate))
					mp.put("ued", "");
				else
					mp.put("ued", UTCEndDate);

				if (IsNullOrEmpty(UTCStartDate))
					mp.put("usd", "");
				else
					mp.put("usd", UTCStartDate);

				return new URL(GenerateURL(mp, "CreateVcardTag"));

			case EDIT :

				if (IsNullOrEmpty(CurrentTitle))
					throw new Exception("The CurrentTile cannot be null or empty when editing a Tag.");
				else
					mp.put("ct", CurrentTitle);

				if (IsNullOrEmpty(CategoryName))
					mp.put("cn", "");
				else
					mp.put("cn", CategoryName);

				if (IsNullOrEmpty(City))
					mp.put("c", "");
				else
					mp.put("c", City);

				if (IsNullOrEmpty(Company))
					mp.put("co", "");
				else
					mp.put("co", Company);
				
				if (IsNullOrEmpty(Country))
					mp.put("cou", "");
				else
					mp.put("cou", Country);
				
				if (IsNullOrEmpty(Email))
					mp.put("e", "");
				else
					mp.put("e", Email);
				
				if (IsNullOrEmpty(Fax))
					mp.put("f", "");
				else
					mp.put("f", Fax);
				
				if (IsNullOrEmpty(Firstname))
					mp.put("fn", "");
				else
					mp.put("fn", Firstname);
				
				if (IsNullOrEmpty(Lastname))
					mp.put("ln", "");
				else
					mp.put("ln", Lastname);
				
				if (IsNullOrEmpty(MobilePhone))
					mp.put("mp", "");
				else
					mp.put("mp", MobilePhone);
				
				if (IsNullOrEmpty(State))
					mp.put("st", "");
				else
					mp.put("st", State);
				
				if (IsNullOrEmpty(Webpage))
					mp.put("w", "");
				else
					mp.put("w", Webpage);
				
				if (IsNullOrEmpty(Workphone))
					mp.put("wp", "");
				else
					mp.put("wp", Workphone);
				
				if (IsNullOrEmpty(Zip))
					mp.put("z", "");
				else
					mp.put("z", Zip);
				
				if (IsNullOrEmpty(Street))
					mp.put("str", "");
				else
					mp.put("str", Street);
				
				if (IsNullOrEmpty(Password))
					mp.put("p", "");
				else
					mp.put("p", Password);

				if (TagStatus == null)
					mp.put("ts", "");
				else
					mp.put("ts", TagStatus.toString());

				if (IsNullOrEmpty(Title))
					throw new Exception("The Title cannot be null or empty when editing a Tag.");
				else
					mp.put("t", Title);

				if (IsNullOrEmpty(UTCEndDate))
					mp.put("ued", "");
				else
					mp.put("ued", UTCEndDate);

				if (IsNullOrEmpty(UTCStartDate))
					mp.put("usd", "");
				else
					mp.put("usd", UTCStartDate);

				return new URL(GenerateURL(mp, "EditVcardTag"));
			case ACTIVE :
				return Activate();
			case PAUSE :
				return Pause();

		}
		return null;
	}
}
