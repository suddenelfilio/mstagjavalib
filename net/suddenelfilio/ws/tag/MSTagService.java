package net.suddenelfilio.ws.tag;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.URL;
import java.io.InputStreamReader;

public final class MSTagService
{

	public static String	SVCURL		= "http://tag.ws.suddenelfilio.net/mstagrest.svc/";
	public static String	SVCAPITOKEN	= "";
	public static String	LASTURL 	= "";

	public final String Create(Object objectToCreate) throws Exception
	{
		return MakeRequest(((TagItemAPIBase) objectToCreate).ToURL(URLType.CREATE));
	}

	public final String Edit(Object objectToEdit) throws Exception
	{
		return MakeRequest(((TagItemAPIBase) objectToEdit).ToURL(URLType.EDIT));
	}

	public final String Pause(Object objectToPause) throws Exception
	{
		return MakeRequest(((TagItemAPIBase) objectToPause).ToURL(URLType.PAUSE));
	}

	public final String Activate(Object objectToActivate) throws Exception
	{
		return MakeRequest(((TagItemAPIBase) objectToActivate).ToURL(URLType.ACTIVE));
	}
	
	public final byte[] GenerateBarcode(BarcodeGenerationSettings settings) throws Exception
	{
		return Base64.decode(MakeRequest(((TagItemAPIBase) settings).ToURL(URLType.GENERATE)));
	}

	private final String MakeRequest(URL requestUrl) throws IOException
	{

		MSTagService.LASTURL = requestUrl.toString();
		BufferedReader in = new BufferedReader(new InputStreamReader(requestUrl.openStream()));
		String inputLine = in.readLine();

		return inputLine;
	}

}
