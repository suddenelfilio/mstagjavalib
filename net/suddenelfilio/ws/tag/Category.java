package net.suddenelfilio.ws.tag;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public final class Category extends TagItemAPIBase
{

	public String	Name;
	public String	CurrentName;
	public Status	CategoryStatus;
	public String	UTCEndDate;
	public String	UTCStartDate;

	@Override
	public URL ToURL(URLType type) throws Exception
	{
		if (MSTagService.SVCAPITOKEN == null || MSTagService.SVCAPITOKEN.equals(""))
			throw new Exception("Did you forget to set the MSTagService.SVCAPITOKEN with your token?");

		Map<String, String> mp = new HashMap<String, String>();
		mp.put("at", MSTagService.SVCAPITOKEN);
		switch (type)
		{
			case CREATE :

				if (IsNullOrEmpty(Name))
					throw new Exception("The Name cannot be null or empty when creating a Tag.");
				else
					mp.put("cn", Name);

				if (CategoryStatus == null)
					mp.put("s", "");
				else
					mp.put("s", CategoryStatus.toString());

				if (IsNullOrEmpty(UTCEndDate))
					mp.put("ued", "");
				else
					mp.put("ued", UTCEndDate);

				if (IsNullOrEmpty(UTCStartDate))
					mp.put("usd", "");
				else
					mp.put("usd", UTCStartDate);

				return new URL(GenerateURL(mp, "CreateCategory"));

			case EDIT :

				if (IsNullOrEmpty(Name))
					throw new Exception("The Name cannot be null or empty when editing a Category.");
				else
					mp.put("ncn", Name);

				if (IsNullOrEmpty(CurrentName))
					throw new Exception("The CurrentName cannot be null or empty when editing a Category.");
				else
					mp.put("ocn", CurrentName);

				if (CategoryStatus == null)
					mp.put("s", "");
				else
					mp.put("s", CategoryStatus.toString());

				if (IsNullOrEmpty(UTCEndDate))
					mp.put("ued", "");
				else
					mp.put("ued", UTCEndDate);

				if (IsNullOrEmpty(UTCStartDate))
					mp.put("usd", "");
				else
					mp.put("usd", UTCStartDate);

				return new URL(GenerateURL(mp, "EditCategory"));
			case ACTIVE :
				if (IsNullOrEmpty(Name))
					throw new Exception("The Name cannot be null or empty when activating a Category.");
				else
					mp.put("cn", Name);

				return new URL(GenerateURL(mp, "ActivateCategory"));
			case PAUSE :
				if (IsNullOrEmpty(Name))
					throw new Exception("The Name cannot be null or empty when pausing a Category.");
				else
					mp.put("cn", Name);

				return new URL(GenerateURL(mp, "PauseCategory"));

		}
		return null;
	}
}
