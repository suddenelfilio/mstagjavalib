package net.suddenelfilio.ws.tag;

public enum ImageTypes
{
	Pdf, Wmf, Jpeg, Png, Gif, Tiff
}
