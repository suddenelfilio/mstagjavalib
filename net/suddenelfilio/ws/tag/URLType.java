package net.suddenelfilio.ws.tag;

public enum URLType
{
	CREATE, EDIT, ACTIVE, PAUSE, GENERATE
}
