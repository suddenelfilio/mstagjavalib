package net.suddenelfilio.ws.tag;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public abstract class TagItemAPIBase
{

	public abstract URL ToURL(URLType type) throws MalformedURLException,
			Exception;

	public Boolean IsNullOrEmpty(String value)
	{
		return (value == null || value.equals(""));
	}

	public Boolean IsNull(String value)
	{
		return (value == null);
	}

	protected String GenerateURL(Map<String, String> map, String action)
			throws UnsupportedEncodingException
	{
		Iterator<Entry<String, String>> it = map.entrySet().iterator();

		String url = String.format("%s%s?", MSTagService.SVCURL, action);

		while (it.hasNext())
		{
			Entry<String, String> e = it.next();
			String value;
			if (!IsNullOrEmpty(e.getValue()))
			{
				value = URLEncoder.encode(e.getValue(), "UTF-8");
				url = String.format("%s%s=%s&", url, e.getKey(), value);
			}
		}

		return url.replaceAll(".$", "");
	}
}
