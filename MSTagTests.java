import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import net.suddenelfilio.ws.tag.*;
import junit.framework.TestCase;

public class MSTagTests extends TestCase
{

	protected void setUp()
	{
		MSTagService.SVCAPITOKEN = "F4-18-38-79-69-B7-EB-C8-E1-14-26-4B-00-77-1D-4F";
	}

	public void testCreateCategory()
	{
		MSTagService svc = new MSTagService();

		Category cat = new Category();

		cat.Name = "Java Created Tags";
		cat.CategoryStatus = Status.Active;
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		cat.UTCStartDate = sdf.format(now);

		try
		{
			String result = svc.Create(cat);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void testEditCategory()
	{
		MSTagService svc = new MSTagService();

		Category cat = new Category();

		cat.CurrentName = "Java Created Tags";
		cat.Name = "JavaTags";
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		cat.UTCStartDate = sdf.format(now);

		try
		{
			String result = svc.Edit(cat);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void testPauseCategory()
	{
		MSTagService svc = new MSTagService();

		Category cat = new Category();
		cat.Name = "JavaTags";

		try
		{
			String result = svc.Pause(cat);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public void testActivateCategory()
	{
		MSTagService svc = new MSTagService();

		Category cat = new Category();
		cat.Name = "JavaTags";

		try
		{
			String result = svc.Activate(cat);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void testCreateFreeTextTag()
	{

		MSTagService svc = new MSTagService();

		FreeTextTag tag = new FreeTextTag();
		tag.CategoryName = "JavaTags";
		tag.Title = "Java FreeText Tag Test";
		tag.FreeText = "this is a java test";
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		tag.UTCStartDate = sdf.format(now);

		try
		{
			String result = svc.Create(tag);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void testEditFreeTextTag()
	{
		MSTagService svc = new MSTagService();

		FreeTextTag tag = new FreeTextTag();
		tag.CategoryName = "JavaTags";
		tag.CurrentTitle = "Java FreeText Tag Test";
		tag.FreeText = "this is a java test";
		tag.Title = "JavaFreeTextTagTest";
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		tag.UTCStartDate = sdf.format(now);

		try
		{
			String result = svc.Edit(tag);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public void testPauseFreeTextTag()
	{
		MSTagService svc = new MSTagService();

		FreeTextTag tag = new FreeTextTag();
		tag.CategoryName = "JavaTags";
		tag.Title = "JavaFreeTextTagTest";

		try
		{
			String result = svc.Pause(tag);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void testActivateFreeTextTag()
	{
		MSTagService svc = new MSTagService();

		FreeTextTag tag = new FreeTextTag();
		tag.CategoryName = "JavaTags";
		tag.Title = "JavaFreeTextTagTest";

		try
		{
			String result = svc.Activate(tag);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void testCreateURITag()
	{

		MSTagService svc = new MSTagService();

		URITag tag = new URITag();
		tag.CategoryName = "JavaTags";
		tag.Title = "Java URI Tag Test";
		tag.MedFiUrl = "http://tag.microsoft.com";
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		tag.UTCStartDate = sdf.format(now);

		try
		{
			String result = svc.Create(tag);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void testEditURITag()
	{
		MSTagService svc = new MSTagService();

		URITag tag = new URITag();
		tag.CategoryName = "JavaTags";
		tag.CurrentTitle = "Java URI Tag Test";
		tag.MedFiUrl = "http://tag.microsoft.com";
		tag.Title = "JavaURITagTest";
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		tag.UTCStartDate = sdf.format(now);

		try
		{
			String result = svc.Edit(tag);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public void testPauseURITag()
	{
		MSTagService svc = new MSTagService();

		URITag tag = new URITag();
		tag.CategoryName = "JavaTags";
		tag.Title = "JavaURITagTest";

		try
		{
			String result = svc.Pause(tag);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void testActivateURITag()
	{
		MSTagService svc = new MSTagService();

		URITag tag = new URITag();
		tag.CategoryName = "JavaTags";
		tag.Title = "JavaURITagTest";

		try
		{
			String result = svc.Activate(tag);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void testCreateDialerTag()
	{

		MSTagService svc = new MSTagService();

		DialerTag tag = new DialerTag();
		tag.CategoryName = "JavaTags";
		tag.Title = "Java Dialer Tag Test";
		tag.PhoneNumber = "+3211000000";
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		tag.UTCStartDate = sdf.format(now);

		try
		{
			String result = svc.Create(tag);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void testEditDialerTag()
	{
		MSTagService svc = new MSTagService();

		DialerTag tag = new DialerTag();
		tag.CategoryName = "JavaTags";
		tag.CurrentTitle = "Java Dialer Tag Test";
		tag.PhoneNumber = "+3211000000";
		tag.Title = "JavaDialerTagTest";
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		tag.UTCStartDate = sdf.format(now);

		try
		{
			String result = svc.Edit(tag);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public void testPauseDialerTag()
	{
		MSTagService svc = new MSTagService();

		DialerTag tag = new DialerTag();
		tag.CategoryName = "JavaTags";
		tag.Title = "JavaDialerTagTest";

		try
		{
			String result = svc.Pause(tag);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void testActivateDialerTag()
	{
		MSTagService svc = new MSTagService();

		DialerTag tag = new DialerTag();
		tag.CategoryName = "JavaTags";
		tag.Title = "JavaDialerTagTest";

		try
		{
			String result = svc.Activate(tag);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void testCreateVCardTag()
	{

		MSTagService svc = new MSTagService();

		VCardTag tag = new VCardTag();
		tag.CategoryName = "JavaTags";
		tag.Title = "Java VCard Tag Test";
		tag.Firstname = "MsTag";
		tag.Lastname = "Java Library";
		tag.Country = "Belgium";
		tag.Company = "Suddenelfilio.net";
		tag.Webpage = "http://mstagjavalib.codeplex.com";
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		tag.UTCStartDate = sdf.format(now);

		try
		{
			String result = svc.Create(tag);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void testEditVCardTag()
	{
		MSTagService svc = new MSTagService();

		VCardTag tag = new VCardTag();
		tag.CategoryName = "JavaTags";
		tag.CurrentTitle = "Java VCard Tag Test";
		tag.Title = "JavaVCardTagTest";
		tag.Firstname = "Java Library";
		tag.Lastname = "for Microsoft Tag";
		tag.Country = "Belgium";
		tag.Company = "Suddenelfilio.net";
		tag.Webpage = "http://mstagjavalib.codeplex.com";
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		tag.UTCStartDate = sdf.format(now);

		try
		{
			String result = svc.Edit(tag);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public void testPauseVCardTag()
	{
		MSTagService svc = new MSTagService();

		VCardTag tag = new VCardTag();
		tag.CategoryName = "JavaTags";
		tag.Title = "JavaVCardTagTest";

		try
		{
			String result = svc.Pause(tag);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void testActivateVCardTag()
	{
		MSTagService svc = new MSTagService();

		VCardTag tag = new VCardTag();
		tag.CategoryName = "JavaTags";
		tag.Title = "JavaVCardTagTest";

		try
		{
			String result = svc.Activate(tag);
			assertTrue(result.equals("\"True\""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void testGenerateBarcode()
	{
		
		MSTagService svc = new MSTagService();

		BarcodeGenerationSettings settings = new BarcodeGenerationSettings();

		settings.CategoryName = "JavaTags";
		settings.TagTitle = "JavaVCardTagTest";
		settings.ImageType = ImageTypes.Jpeg;
		settings.Decoration = TagDecorations.HCCBRP_DECORATION_NONE;
		settings.Size = 0.75;

		try
		{

			byte[] barcode = svc.GenerateBarcode(settings);

			FileOutputStream fos = new FileOutputStream("barcode.jpg");

			fos.write(barcode);
			fos.close();

			File f = new File("barcode.jpg");
			assertTrue(f.exists());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
